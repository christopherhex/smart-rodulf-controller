/*
 * Arduino IKEA Rodulf Controller
 */
#include <Arduino.h>
#include <Wire.h>
#include <vl53l1_class.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <assert.h>
#include <stdlib.h>
#include <EEPROM.h>

#define DEV_I2C Wire
#define SerialPort Serial

// Define the different PINS
#define PIN_MOTOR_UP 27
#define PIN_MOTOR_DOWN 12
#define PIN_BUTTON_UP 15
#define PIN_BUTTON_DOWN 33

// Number of bytes to store in EEPROM
#define EEPROM_SIZE 2
// EEPROM address to store sit level
#define D_EEPROM_SIT 0
// EEPROM address to store stand level
#define D_EEPROM_STAND 1


// Components.
VL53L1 *sensor_vl53l1_sat;


enum BTN_STATE { OFF = 1, SINGLE = 2, MULTI = 3, LONG = 4 };

#define BTN_ON_STATE 0
#define BTN_OFF_STATE 1
#define SHORT_MS 500

/*
 * button_handler
 * --------------
 * This app handles 
 */
struct button_handler {
  button_handler() : start_time(0), last_value(BTN_OFF_STATE), taps(0)  {}
  unsigned long start_time;
  int last_value;
  unsigned int taps;
  BTN_STATE add_value(int val)
  {
    BTN_STATE ret_val;
    
    unsigned long diff = millis() - start_time;

    if(val == BTN_OFF_STATE && last_value == BTN_OFF_STATE ){
      // STEADY OFF
      if(start_time != 0 && diff > SHORT_MS){
        if(taps == 0) {
          // Off, after timeout, no taps
          ret_val = OFF;
        } else if (taps == 1){
          ret_val = SINGLE;
        } else if (taps > 1){
          ret_val = MULTI;
        }

        start_time = 0;
        taps = 0;
      } else {
         ret_val = OFF;
      }

      
    } else if(val == BTN_ON_STATE && last_value == BTN_ON_STATE){
      // STEADY ON

      if(diff > SHORT_MS){
        ret_val = LONG;
      } else {
        ret_val = OFF;
      }
  

      
    } else if (val == BTN_ON_STATE && last_value == BTN_OFF_STATE) {
      // PUSH BUTTON
      // Determine if we have to start new cycle, i.e. update tap
//      if(diff > SHORT_MS){
//        //RESET
//      }
      Serial.println(diff);
      if(start_time == 0){
        start_time = millis();
      }
      
      ret_val = OFF;
      
    } else if (val == BTN_OFF_STATE && last_value == BTN_ON_STATE) {
      // LET GO BUTTON 

      if(diff > SHORT_MS){
        start_time = 0;
      } else {
        Serial.println("ADD_TAP");
        taps++;  
      }

      ret_val = OFF;
    }

    last_value = val;
    return ret_val;
  }
};


button_handler up_button;
button_handler down_button;


/* Setup ---------------------------------------------------------------------*/

void setup()
{
   // Initialize EEPROM
   EEPROM.begin(EEPROM_SIZE);
   
   // Configure I/O
   pinMode(PIN_MOTOR_UP, OUTPUT);
   pinMode(PIN_MOTOR_DOWN, OUTPUT);

   pinMode(PIN_BUTTON_UP, INPUT);
   pinMode(PIN_BUTTON_DOWN, INPUT);
   
   // Initialize serial for output.
   SerialPort.begin(115200);
   SerialPort.println("Starting...");

   // Initialize I2C bus.
   DEV_I2C.begin();

   // Create VL53L1 satellite component.
   SerialPort.println("Create...");

   sensor_vl53l1_sat = new VL53L1(&DEV_I2C, A1, A2);

   SerialPort.println("Switch Off...");
   // Switch off VL53L1 satellite component.
   sensor_vl53l1_sat->VL53L1_Off();

   SerialPort.println("Init Sensor...");
   //Initialize VL53L1 satellite component.
   sensor_vl53l1_sat->InitSensor(0x12);

   
    
   VL53L1_RoiConfig_t RoiConfig;
   RoiConfig.NumberOfRoi = 1;
   RoiConfig.UserRois[0].TopLeftX = 7;
   RoiConfig.UserRois[0].TopLeftY = 10;
   RoiConfig.UserRois[0].BotRightX = 10;
   RoiConfig.UserRois[0].BotRightY = 7;
   VL53L1_RoiConfig_t *pRoiConfig = &RoiConfig;
   
   sensor_vl53l1_sat->VL53L1_SetROI(pRoiConfig);

   // Start Measurements
   SerialPort.println("Start Ranging...");

   sensor_vl53l1_sat->VL53L1_SetPresetMode(VL53L1_PRESETMODE_RANGING);
   int status;
   status = sensor_vl53l1_sat->VL53L1_ClearInterruptAndStartMeasurement();

   SerialPort.println(status);
}


uint32_t GetMeasurement()
{
  uint32_t RangeVal; 
  int status;

  do {
    status = sensor_vl53l1_sat->GetDistance(&RangeVal);
  } while (status != 0);

  return RangeVal;
}


void GoUp(){
  digitalWrite(PIN_MOTOR_DOWN, LOW);
  digitalWrite(PIN_MOTOR_UP, HIGH);
}

void GoDown(){
  digitalWrite(PIN_MOTOR_UP, LOW);
  digitalWrite(PIN_MOTOR_DOWN, HIGH);
}

void Stop(){
  digitalWrite(PIN_MOTOR_DOWN, LOW);
  digitalWrite(PIN_MOTOR_UP, LOW);
}

int autoMode = 0;
uint32_t targetHeight;
uint32_t height;


void StoreStanding(uint32_t height){
  //make sure that it's an achievable level
  EEPROM.write(D_EEPROM_STAND, height/10);
  EEPROM.commit(); 

}

uint32_t GetStanding(){
  int standValue = EEPROM.read(D_EEPROM_STAND);
  return standValue * 10;
}

void StoreSit(uint32_t height){
  //make sure that it's an achievable level
  EEPROM.write(D_EEPROM_SIT, height/10);
  EEPROM.commit(); 

}

uint32_t GetSit(){
  int sitValue = EEPROM.read(D_EEPROM_SIT);
  return sitValue * 10;
}

/*
 * loop()
 */
void loop()
{

  BTN_STATE upRes, downRes;
  int upVal, downVal;

  
  height = GetMeasurement();

  upVal = digitalRead(PIN_BUTTON_UP);
  downVal = digitalRead(PIN_BUTTON_DOWN);
  upRes = up_button.add_value(upVal);
  downRes = down_button.add_value(downVal);
  

  // Automode means that we're transitioning to a certain preset-height
  if(autoMode != 0 ){

    int diff = targetHeight - height;
    Serial.print("Target:");
    Serial.print(targetHeight);
    Serial.print(" Current:");
    Serial.print(height);
    Serial.print(" DIFF:");
    Serial.println(diff);

    // +1 means rising
    if(autoMode == 1 && diff > 0){
      Serial.println("AUTORISE");
      GoUp();
    } else if(autoMode == -1 && diff < 0 ){
      Serial.println("AUTODOWN");
      GoDown();
    } else {
      Stop();
      autoMode = 0;
    }
  }

  
  if(upRes == SINGLE){
    Serial.println("UP - SINGLE");

    if(autoMode != 0){
      Serial.println("STOP MOVING");
      autoMode = 0;
    } else {
      autoMode = 1;
      targetHeight = GetStanding();
      Serial.print("Move UP to ");
      Serial.println(targetHeight);

    }
  } else if (downRes == SINGLE) {
    Serial.println("DOWN - SINGLE");

    if(autoMode != 0){
      Serial.println("STOP MOVING");
      autoMode = 0;
    } else {
      targetHeight = GetSit();
      autoMode = -1;
      Serial.print("Move DOWN to ");
      Serial.println(targetHeight);
    }  
  } else if (upRes == MULTI) {
    Serial.print("Store UP value");
    Serial.println(height);
    StoreStanding(height);
  } else if (downRes == MULTI) {
    Serial.print("Store DOWN value");
    Serial.println(height);
    StoreSit(height);
  } else if(upRes == LONG){
    Serial.print("GO UP ");
    Serial.println(height);
    GoUp();
  } else if (downRes == LONG){
    Serial.print("GO DOWN ");
    Serial.println(height);
    GoDown();
  } else {
    if(autoMode == 0){
      Stop();
    }
  }
  
}
