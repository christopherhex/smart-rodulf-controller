# smart-rodulf-controller

IKEA Rodulf smart controller that is able to persist height.




## Hardware
* [Adafruit HUZZAH32 - ESP32 Feather Board](https://www.adafruit.com/product/3405)
* [Time-of-Flight Sensor - VL53L1 Breakout board](https://www.st.com/en/evaluation-tools/vl53l1-satel.html)

## Controller



## Enclosure

The enclosure is designed using Autodesk Fusion 360

